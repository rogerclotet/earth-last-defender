extends Actor
class_name Player


export var acceleration = Vector2(300, 300)
export var max_speed = 200.0
export var drag = 1.5
export var shots = 1
export var spread = 10

var floating_text = preload("res://UserInterface/FloatingText.tscn")

onready var throttle_sprite = $ThrottleSprite
onready var audio_players = [
	$AudioStreamPlayer2D,
	$AudioStreamPlayer2D2,
	$AudioStreamPlayer2D3,
	$AudioStreamPlayer2D4,
	$AudioStreamPlayer2D5,
]
onready var damage_audio_player = $DamageAudioStreamPlayer2D

var _velocity = Vector2(0, 0)


func _process(_delta: float) -> void:
	look_at(get_global_mouse_position())


func _physics_process(delta: float) -> void:
	if Input.is_action_pressed("fire") and not _on_cooldown:
		_fire()

	_process_movement(delta)


func _fire() -> void:
	if !Input.is_action_pressed("fire") or _on_cooldown:
		return

	var total_spread = spread * (shots - 1)
	var base_angle = -total_spread / 2.0

	for i in range(0, shots):
		var angle = base_angle + spread * i + rand_range(-2.0, 2.0)
		_fire_bullet(angle)

	_play_pew_sound()
	_reset_cooldown()


func _fire_bullet(angle: float) -> void:
	var b = bullet.instance()
	b.global_position = global_position
	b.rotation = rotation + deg2rad(angle)
	get_parent().add_child(b)



func _play_pew_sound() -> void:
	for player in audio_players:
		if !player.playing:
			player.play()
			return


func _process_movement(delta: float) -> void:
	var direction = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	)

	throttle_sprite.play("idle" if direction == Vector2.ZERO else "throttle")

	_velocity += direction * acceleration * delta
	_velocity = _velocity.normalized() * clamp(_velocity.length() - drag, 0, max_speed)

	_velocity = move_and_slide(_velocity)


func _take_damage(damage: int) -> void:
	PlayerData.player_health -= damage

	var ft = floating_text.instance()
	ft.text = damage
	ft.color = "cf2828"
	add_child(ft)

	damage_audio_player.play()


func _die() -> void:
	print("game over")

	$CollisionPolygon2D.disabled = true

	var sprite = $ShipSprite
	sprite.play("death")
	yield(sprite, "animation_finished")

	get_tree().root.get_node("Game").get_node("Camera2D").current = true

	queue_free()
