extends Actor


export var shots: float = 1
export var spread: float
export var fire_range: float
export var acceleration: float
export var max_speed: float
export var drag: float = 1.5
export var turn_speed: float
export var score_value: int

const TARGET = Vector2.ZERO

enum State { ALIVE, DEAD }

var floating_text = preload("res://UserInterface/FloatingText.tscn")

onready var fire_player = $FireAudioPlayer
onready var damage_player = $DamageAudioPlayer
onready var animation_player = $AnimationPlayer

var _state = State.ALIVE
var _velocity = Vector2.ZERO


func _physics_process(delta: float) -> void:
	if _state == State.DEAD:
		return

	var distance = global_position.distance_to(TARGET)

	if distance > fire_range:
		_face_target(delta)
		_move_to_target(delta)
	else:
		_face_target(delta)
		_try_to_fire()

	_velocity = _velocity.normalized() * max(_velocity.length() - drag, 0)
	_velocity = move_and_slide(_velocity)


func _face_target(delta: float) -> void:
	var angle = TARGET.angle_to_point(global_position)
	global_rotation = lerp_angle(global_rotation, angle, delta * turn_speed)


func _move_to_target(delta: float) -> void:
	var vector = TARGET - global_position
	if _velocity.length() < max_speed and vector.dot(Vector2(1, 0).rotated(rotation)) > 1:
		_velocity += Vector2(acceleration, 0).rotated(rotation) * delta


func _try_to_fire() -> void:
	var target_angle = TARGET.angle_to_point(global_position)
	if abs(target_angle - rotation) > 0.1:
		return

	if not _on_cooldown:
		var total_spread = spread * (shots - 1)
		var base_angle = -total_spread / 2.0

		for i in range(0, shots):
			var angle = base_angle + spread * i + rand_range(-2.0, 2.0)
			_fire_bullet(angle)

		fire_player.play()
		_reset_cooldown()


func _fire_bullet(angle: float) -> void:
	var b = bullet.instance()
	b.global_position = global_position
	b.rotation = rotation + deg2rad(angle)
	get_parent().add_child(b)


func _take_damage(damage: int) -> void:
	damage_player.play()

	animation_player.stop()
	animation_player.play("Take Damage")

	var ft = floating_text.instance()
	ft.text = damage
	ft.color = "edd23f"
	add_child(ft)


func _die() -> void:
	$CollisionShape2D.disabled = true

	PlayerData.enemies_killed += 1
	PlayerData.score += score_value

	var animated_sprite = $AnimatedSprite
	animated_sprite.play("death")
	yield(animated_sprite, "animation_finished")

	queue_free()
