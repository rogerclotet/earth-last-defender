extends Area2D


var floating_text = preload("res://UserInterface/FloatingText.tscn")

onready var sprite = $AnimatedSprite
onready var damage_audio_player = $AudioStreamPlayer2D


func _on_Earth_body_entered(body: Node) -> void:
	if body is Bullet:
		var damage = int(rand_range(body.min_damage, body.max_damage))
		PlayerData.earth_health -= damage

		body.queue_free()

		var ft = floating_text.instance()
		ft.text = damage
		ft.color = "cf2828"
		add_child(ft)

		damage_audio_player.play()

		# TODO screen shake

		if PlayerData.earth_health <= 0:
			sprite.play("death")
			yield(sprite, "animation_finished")

			PlayerData.save_game()
			get_tree().change_scene("res://Scenes/EndScreen.tscn")
