extends Node2D


export (Array, PackedScene) var power_ups
export var min_spawn_cooldown = 3.0
export var max_spawn_cooldown = 10.0
export var spawn_range = 20.0
export var spawn_velocity = 100.0
export var spawn_velocity_variation = 30.0

var _timer: Timer


func _init() -> void:
	_timer = Timer.new()
	add_child(_timer)

	var err = _timer.connect("timeout", self, "_spawn_power_up")
	if err:
		print_debug("error connecting power up spawn cooldown timer timeout: " + err)


func _ready() -> void:
	randomize()
	_reset_timer()


func _reset_timer() -> void:
	var cooldown = rand_range(min_spawn_cooldown, max_spawn_cooldown)
	_timer.start(cooldown)


func _spawn_power_up() -> void:
	var spawn_rotation = randf() * TAU
	var direction = Vector2(1, 0).rotated(spawn_rotation)
	var power_up = power_ups[rand_range(0, power_ups.size())].instance()
	power_up.global_position = direction * spawn_range
	power_up.velocity = direction * rand_range(spawn_velocity - spawn_velocity_variation, spawn_velocity + spawn_velocity_variation)
	add_child(power_up)

	_reset_timer()
