extends KinematicBody2D
class_name Actor


export var max_health: int
export var bullet: PackedScene
export var fire_rate = 2.0

onready var health = max_health

var _cooldown_timer: Timer
var _on_cooldown = false


func _init() -> void:
	_cooldown_timer = Timer.new()
	add_child(_cooldown_timer)

	var err = _cooldown_timer.connect("timeout", self, "_cooldown_timer_timeout")
	if err:
		print_debug("error connecting cooldown timer timeout: " + err)


func hit(damage: int) -> void:
	_take_damage(damage)
	health -= damage

	if health <= 0:
		_die()


func _take_damage(_damage: int) -> void:
	pass


func _die() -> void:
	pass


func _reset_cooldown() -> void:
	_on_cooldown = true
	_cooldown_timer.start(1 / fire_rate)


func _cooldown_timer_timeout() -> void:
	_on_cooldown = false
