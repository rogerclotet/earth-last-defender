extends Control


onready var label = $Label
onready var bar = $Bar
onready var INITIAL_SIZE = bar.rect_size.x


func _ready() -> void:
	update()
	PlayerData.connect("earth_health_updated", self, "update")


func update() -> void:
	_update_label()
	_update_bar()


func _update_label() -> void:
	label.text = "%d/%d" % [PlayerData.earth_health, PlayerData.MAX_EARTH_HEALTH]


func _update_bar() -> void:
	var percent = PlayerData.earth_health / float(PlayerData.MAX_EARTH_HEALTH)
	bar.rect_size.x = INITIAL_SIZE * percent
