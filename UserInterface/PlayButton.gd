extends Button


export var game: PackedScene


func _pressed() -> void:
	PlayerData.reset()
	get_tree().change_scene_to(game)
