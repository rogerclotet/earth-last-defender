extends Button


func _pressed() -> void:
	PlayerData.save_game() # Just in case this is pressed from the pause menu and high score was updated

	var tree = get_tree()
	tree.paused = false
	tree.change_scene("res://Scenes/MainMenu.tscn")
