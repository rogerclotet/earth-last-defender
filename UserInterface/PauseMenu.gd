extends Control


var _paused = false


func _ready() -> void:
	visible = false


func _input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("pause"):
		if _paused:
			resume()
		else:
			pause()

		_paused = !_paused


func pause() -> void:
	get_tree().paused = true
	visible = true


func resume() -> void:
	visible = false
	get_tree().paused = false
