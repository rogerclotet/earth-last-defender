extends Label


func _ready() -> void:
	update_displayed_score()
	PlayerData.connect("score_updated", self, "update_displayed_score")


func update_displayed_score() -> void:
	text = "Score: %d" % PlayerData.score
