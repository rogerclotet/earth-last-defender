extends Position2D


onready var label = $Label
onready var tween = $Tween
var text = ""
var color = "ffffff"

var _velocity = Vector2.ZERO


func _ready() -> void:
	label.text = str(text)
	label.set("custom_colors/font_color", Color(color))
	global_rotation = 0

	var side_movement = randi() % 41 - 20
	_velocity = Vector2(side_movement, 50)

	tween.interpolate_property(self, "scale", scale, Vector2.ONE, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(self, "scale", Vector2.ONE, Vector2(0.1, 0.1), 0.7, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0.3)
	tween.start()


func _on_Tween_tween_all_completed() -> void:
	queue_free()


func _process(delta: float) -> void:
	global_position -= _velocity * delta
