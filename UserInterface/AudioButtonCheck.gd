extends CheckButton


func _ready() -> void:
	pressed = Settings.audio_enabled
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), !Settings.audio_enabled)


func _pressed() -> void:
	Settings.audio_enabled = pressed
	Settings.save_settings()

	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), !pressed)
