extends Label


func _ready() -> void:
	update_displayed_high_score()
	PlayerData.connect("high_score_updated", self, "update_displayed_high_score")


func update_displayed_high_score() -> void:
	text = "High Score: %d" % PlayerData.high_score
