extends Node


signal earth_health_updated
signal player_health_updated
signal score_updated
signal high_score_updated


const MAX_EARTH_HEALTH = 200
const MAX_PLAYER_HEALTH = 100
const SAVE_FILE_NAME = "user://save.bru"


var earth_health: int = MAX_EARTH_HEALTH setget set_earth_health
var player_health: int = MAX_PLAYER_HEALTH setget set_player_health
var score: int setget set_score
var high_score: int
var enemies_killed: int


func _ready() -> void:
	load_game()


func save_game() -> void:
	var save_game = File.new()
	save_game.open(SAVE_FILE_NAME, File.WRITE)
	var save_data = {
		"high_score": high_score
	}
	save_game.store_line(to_json(save_data))
	save_game.close()


func load_game() -> void:
	var save_game = File.new()
	if not save_game.file_exists(SAVE_FILE_NAME):
		return

	save_game.open(SAVE_FILE_NAME, File.READ)
	var save_data = parse_json(save_game.get_line())

	if save_data.high_score:
		high_score = save_data.high_score

	save_game.close()


func reset() -> void:
	earth_health = MAX_EARTH_HEALTH
	player_health = MAX_PLAYER_HEALTH
	score = 0
	enemies_killed = 0


func set_earth_health(value: int) -> void:
	earth_health = max(0, value)
	emit_signal("earth_health_updated")


func set_player_health(value: int) -> void:
	player_health = clamp(value, 0, MAX_PLAYER_HEALTH)
	emit_signal("player_health_updated")


func set_score(value: int) -> void:
	score = value

	if score > high_score:
		high_score = score
		emit_signal("high_score_updated")

	emit_signal("score_updated")
