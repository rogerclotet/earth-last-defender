extends Node


const SETTINGS_FILE_NAME = "user://settings.bru"


var audio_enabled = true


func _ready() -> void:
	load_settings()


func save_settings() -> void:
	var saved_settings = File.new()
	saved_settings.open(SETTINGS_FILE_NAME, File.WRITE)
	var save_data = {
		"audio_enabled": audio_enabled
	}
	saved_settings.store_line(to_json(save_data))
	saved_settings.close()


func load_settings() -> void:
	var saved_settings = File.new()
	if not saved_settings.file_exists(SETTINGS_FILE_NAME):
		return

	saved_settings.open(SETTINGS_FILE_NAME, File.READ)
	var save_data = parse_json(saved_settings.get_line())

	if save_data.has("audio_enabled"):
		audio_enabled = save_data.audio_enabled

	saved_settings.close()
