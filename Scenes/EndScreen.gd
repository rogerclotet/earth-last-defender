extends Control


onready var score_label = $VBoxContainer/Score


func _ready() -> void:
	score_label.bbcode_text = \
		"[center]Your score was [color=white]%d[/color]!\nYou defeated [color=red]%d[/color] enemies[/center]" \
		% [PlayerData.score, PlayerData.enemies_killed]
