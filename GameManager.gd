extends Node2D


export var spawn_cooldown = 5.0
export var min_cooldown = 0.25
export var spawn_increase_count = 7
export var spawn_safe_radius = 300
export var enemy_health_scaling = 1
export var enemy_health_scaling_count = 20
export var enemy_health_scaling_increase = 0.15
export (Array, PackedScene) var enemies


var _spawn_timer: Timer
var _spawned_enemies = 0


func _init() -> void:
	_spawn_timer = Timer.new()
	add_child(_spawn_timer)

	var err = _spawn_timer.connect("timeout", self, "_spawn_timer_timeout")
	if err:
		print_debug("error connecting spawn timer timeout: " + err)


func _ready() -> void:
	_spawn_enemy()
	_spawn_timer.start(spawn_cooldown)


func _spawn_timer_timeout() -> void:
	_spawn_enemy()
	_spawned_enemies += 1

	if spawn_cooldown > min_cooldown and _spawned_enemies % spawn_increase_count == 0:
		spawn_cooldown = max(min_cooldown, spawn_cooldown - 0.5)

	if _spawned_enemies % enemy_health_scaling_count == 0:
		enemy_health_scaling += enemy_health_scaling_increase

	_spawn_timer.start(spawn_cooldown)


func _spawn_enemy() -> void:
	var enemy = enemies[randi() % enemies.size()].instance()
	enemy.max_health *= enemy_health_scaling
	enemy.global_position = _get_valid_spawn_position()
	enemy.rotation = randf() * TAU
	add_child(enemy)


func _get_valid_spawn_position() -> Vector2:
	while true:
		var pos = Vector2(randi() % 1400 - 700, randi() % 1400 - 700)
		if pos.distance_to(Vector2.ZERO) >= spawn_safe_radius:
			return pos

	return Vector2(-250, 0)
