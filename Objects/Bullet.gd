extends KinematicBody2D


class_name Bullet


export var min_damage: float
export var max_damage: float
export var speed = 500.0

var _velocity = Vector2.ZERO


func _ready() -> void:
	_velocity = Vector2(speed, 0).rotated(rotation)


func _physics_process(delta: float) -> void:
	if not _velocity:
		return

	var collision = move_and_collide(_velocity * delta)
	if collision:
		var collider = collision.collider
		if collider is Actor:
			collider.hit(rand_range(min_damage, max_damage))
		queue_free()
