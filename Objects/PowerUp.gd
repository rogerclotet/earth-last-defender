extends Node2D
class_name PowerUp


const drag = 0.5

var velocity: Vector2


func _ready() -> void:
	$AnimationPlayer.play("PowerUp")


func _physics_process(delta: float) -> void:
	velocity -= velocity * drag * delta
	global_position += velocity * delta
