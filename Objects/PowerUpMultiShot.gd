extends PowerUp


onready var audio_stream_player = $AudioStreamPlayer2D
onready var sprite = $Sprite


func _on_PowerUpMultiShot_body_entered(body: Node) -> void:
	if body is Player:
		body.shots += 1

		sprite.visible = false
		audio_stream_player.play()
		yield(audio_stream_player, "finished")
		queue_free()
