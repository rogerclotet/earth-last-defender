extends PowerUp


onready var audio_stream_player = $AudioStreamPlayer2D
onready var sprite = $Sprite


func _on_PowerUpHeal_body_entered(body: Node) -> void:
	if body is Player:
		PlayerData.player_health += 10

		sprite.visible = false
		audio_stream_player.play()
		yield(audio_stream_player, "finished")
		queue_free()
