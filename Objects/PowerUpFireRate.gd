extends PowerUp


onready var audio_stream_player = $AudioStreamPlayer2D
onready var sprite = $Sprite


func _on_PowerUpFireRate_body_entered(body: Node) -> void:
	if body is Player:
		body.fire_rate += 0.5

		sprite.visible = false
		audio_stream_player.play()
		yield(audio_stream_player, "finished")
		queue_free()
